import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Login extends JFrame {
	static Login frame;
	// DBConnection conn;
	ResultSet rt;
	PreparedStatement prt;

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public Login() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "sanjsi");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 884, 516);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUserLogin = new JLabel("User Login");
		lblUserLogin.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblUserLogin.setBounds(378, 185, 126, 32);
		contentPane.add(lblUserLogin);

		JLabel label = new JLabel("User Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(324, 266, 126, 16);
		contentPane.add(label);

		JLabel label_1 = new JLabel("Password:");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_1.setBounds(324, 308, 126, 16);
		contentPane.add(label_1);

		textField = new JTextField();
		textField.setBounds(426, 265, 117, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(426, 307, 117, 20);
		contentPane.add(textField_1);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String fname = textField.getText();
				String pass = textField_1.getText();

				try {

					String sql = "select * from library.libraryes where Firstname='" + fname + "' and Password='" + pass
							+ "'";
					// PreparedStatement pst;
					// pst = conn.prepareStatement(sql);
					// pst.setString(1, textField.getText());
					// pst.setString(2,textField_1.getText());\
					// ResultSet rs;
					// rs = pst.executeQuery();
					// PreparedStatement pst = conn.prepareStatement(sql);
					// pst.setString(1, textField.getText());
					// pst.setString(2, textField_1.getText());
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					if (rs.next()) {
						// rs.close();
						// pst.close();

						setVisible(false);
						ViewSignUp vw = new ViewSignUp();
						vw.setVisible(true);
					} else {
						JOptionPane.showMessageDialog(null, "Error");
					}

				} catch (Exception e) {
					System.out.println(e);
					// TODO: handle exception
				}
				// String name = textField.getText();
				// String password = String.valueOf(textField_1.getText());
				// if (name.equals("admin") && password.equals("admin123")) {
				// SignUp.main(new String[] {});
				// frame.dispose();

				// } else {
				// JOptionPane.showMessageDialog(Login.this, "Sorry, Username or Password
				// Error", "Login Error!",
				// JOptionPane.ERROR_MESSAGE);
				// textField.setText("");
				// textField_1.setText("");
				// }
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogin.setBounds(324, 361, 89, 23);
		contentPane.add(btnLogin);

		JButton button = new JButton("Signup");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SignUp.main(new String[] {});

			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD, 13));
		button.setBounds(444, 362, 89, 23);
		contentPane.add(button);

		JLabel lblNewLabel = new JLabel("New label");
		Image img = new ImageIcon(this.getClass().getResource("book.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(-21, 0, 928, 184);
		contentPane.add(lblNewLabel);
	}
}
