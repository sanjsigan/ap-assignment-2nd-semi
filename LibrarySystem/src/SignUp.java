import java.awt.BorderLayout;
import java.sql.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class SignUp extends JFrame {
	static SignUp frame;
	protected static final String JTextField = null;
	// DBConnection conn;
	// ResultSet rt;
	// PreparedStatement prt;

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignUp frame = new SignUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;
	private JTable table;

	public SignUp() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "sanjsi");
		JOptionPane.showMessageDialog(null, "Connected to Database");
		// super("Login");
		// getComponents();
		// nn = (DBConnection) DBConnection.ConnecrDb();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 515, 305);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCreateNewAccount = new JLabel("Create New Account");
		lblCreateNewAccount.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCreateNewAccount.setBounds(175, 11, 195, 30);

		contentPane.add(lblCreateNewAccount);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblFirstName.setBounds(25, 76, 77, 16);
		contentPane.add(lblFirstName);

		JLabel label = new JLabel("Last Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 13));
		label.setBounds(25, 107, 77, 16);
		contentPane.add(label);

		JLabel label_1 = new JLabel("Gender:");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_1.setBounds(25, 134, 77, 16);
		contentPane.add(label_1);

		JLabel label_2 = new JLabel("Age:");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_2.setBounds(25, 161, 77, 16);
		contentPane.add(label_2);

		JLabel label_3 = new JLabel("Password:");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_3.setBounds(25, 188, 77, 16);
		contentPane.add(label_3);

		textField = new JTextField();
		textField.setBounds(139, 75, 122, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(139, 106, 122, 20);
		contentPane.add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(139, 134, 122, 20);
		contentPane.add(textField_2);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(139, 160, 122, 20);
		contentPane.add(textField_3);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(139, 191, 122, 20);
		contentPane.add(textField_4);

		JButton btnSignup = new JButton("Signup");
		btnSignup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ar0) {
				String fname = textField.getText();
				String flname = textField_1.getText();
				String fgen = textField_2.getText();
				String fage = textField_3.getText();
				String fpass = textField_4.getText();
				try {
					// Connection conn = DBConnection.ConnecrDb();
					String sql = ("insert into library.libraryes  (Firstname,Lastname,Gender,Age,Password) values('"
							+ fname + "','" + flname + "','" + fgen + "','" + fage + "','" + fpass + "')");
					Statement st = (Statement) conn.createStatement();
					st.executeUpdate(sql); 
					/// ps = conn.prepareStatement(sql);
					/**
					 * ps.setString(1, textField.getText()); ps.setString(2, textField_1.getText());
					 * ps.setString(3, textField_2.getText()); ps.setString(4, '
					 * textField_3.getText()); ps.setString(5, textField_4.getText());
					 **/

					JOptionPane.showMessageDialog(null, "New Account created");
					// rt.close();
					// prt.close();
				} catch (Exception e) {
					System.out.println(e);
				}

			}

		});
		btnSignup.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSignup.setBounds(134, 222, 89, 23);
		contentPane.add(btnSignup);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login.main(new String[] {});

			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnBack.setBounds(252, 223, 89, 23);
		contentPane.add(btnBack);

		table = new JTable();
		table.setBounds(442, 134, -95, -71);
		contentPane.add(table);

	}
}
