import java.awt.BorderLayout;
import java.sql.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jcp.xml.dsig.internal.dom.DOMUtils;

import net.proteanit.sql.DbUtils;

import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewSignUp extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewSignUp frame = new ViewSignUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public ViewSignUp() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "sanjsi");
		System.out.println("DWDDQQ");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 853, 438);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable();
		table.setBounds(94, 11, 638, 233);
		contentPane.add(table);

		JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String sql = ("select * from library.libraryes");
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(sql);
					DefaultTableModel tm = (DefaultTableModel) table.getModel();
					tm.setRowCount(0);

					table.setModel(DbUtils.resultSetToTableModel(rs));
					JOptionPane.showMessageDialog(null, "Your Account Successfully Deleted..");

				} catch (Exception e) {
					System.out.println(e);
				}

			}
		});
		btnView.setBounds(375, 296, 89, 23);
		contentPane.add(btnView);
	}
}
